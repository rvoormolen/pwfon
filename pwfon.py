#!/usr/bin/env python3
# Maak wachtwoorden uitspreekbaar voor 'normale' mensen
# 
import sys

# Het alfabet
spel = {
    'A': 'Anna',
    'B': 'Bernard',
    'C': 'Cornelis',
    'D': 'Dirk',
    'E': 'Eduard',
    'F': 'Ferdinand',
    'G': 'Gerard',
    'H': 'Hendrik',
    'I': 'Izaak',
    'J': 'Johannes',
    'K': 'Karel',
    'L': 'Lodewijk',
    'M': 'Maria',
    'N': 'Nico',
    'O': 'Otto',
    'P': 'Pieter',
    'Q': 'Quotient',
    'R': 'Rudolf',
    'S': 'Simon',
    'T': 'Tinus',
    'U': 'Utrecht',
    'V': 'Victor',
    'W': 'Wilem',
    'X': 'Xantippe',
    'Y': 'Ypsilon',
    'Z': 'Zacharias',
    '0': 'Cijfer Nul',
    '1': 'Cijfer Een',
    '2': 'Cijfer Twee',
    '3': 'Cijfer Drie',
    '4': 'Cijfer Vier',
    '5': 'Cijfer Vijf',
    '6': 'Cijfer Zes',
    '7': 'Cijfer Zeven',
    '8': 'Cijfer Acht',
    '9': 'Cijfer Negen',
    '@': 'Apenstaartje',
    '#': 'Hekje',
    '-': 'Min',
    '.': 'Punt',
    ':': 'Dubbelepunt',
    ';': 'Puntkomma',
    '!': 'Uitroepteken',
    '$': 'Dollarteken',
    '%': 'Procent',
    '^': 'Dakje',
    '&': 'Ampersand',
    '?': 'Vraagteken'
}

def pwfon( password ):
    """ Maak er een mooie string van en stuur deze terug """
    output = [ ]
    for char in password:
        try:
            if not char.isalpha() or char.isdigit():
                output.append( '%s' % spel[char.upper()] )
            elif char.isupper():
                output.append( '%s' % spel[char.upper()].upper() )
            else:
                output.append( '%s' % spel[char.upper()].lower() )
        except KeyError:
            output.append( '%s' % char )

    return '%s: %s' % ( password, ' - '.join( output ) )

def main():
    """ Functie welke uitgevoerd wordt als je code als applicatie aanroept """
    while 1:
        try:
            line = sys.stdin.readline()
        except KeyboardInterrupt:
            break

        if not line:
            break
        print( pwfon( line.strip() ) )

if __name__ == '__main__':
    """ We zijn een applicatie """
    main()
